<?php
function tukar_besar_kecil($string){
//kode di sini

$hurufExtend = "";
    foreach(str_split($string) as $str){
        if(ctype_lower($str)){
            $hurufExtend .= strtoupper($str);
        }else if(ctype_upper($str)){
            $hurufExtend .= strtolower($str);
        }else if(ctype_space($str)){
            $hurufExtend .= $str;
        }else if(ctype_digit($str)){
            $hurufExtend .= $str;
        }else if(ctype_graph($str)){
            $hurufExtend .= $str;
        }
    }
    return $string." :  ".$hurufExtend."<br>" ;
}


// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>